﻿//======================================================================================================
// Copyright 2016, NaturalPoint Inc.
//======================================================================================================

using System;
using UnityEngine;


public class RigidBodyTube : MonoBehaviour
{
    public OptitrackStreamingClient StreamingClient;
    public Int32 RigidBodyId, DoigtId;
    public Vector3 offset;
    public Vector3 min, max;
    public Vector3 PMin, PMax;
    public Quaternion offsetRotation;
    private Vector3 A, B, C, O, n;



    void Start()
    {
        // If the user didn't explicitly associate a client, find a suitable default.
        if (this.StreamingClient == null)
        {
            this.StreamingClient = OptitrackStreamingClient.FindDefaultClient();

            // If we still couldn't find one, disable this component.
            if (this.StreamingClient == null)
            {
                Debug.LogError(GetType().FullName + ": Streaming client not set, and no " + typeof(OptitrackStreamingClient).FullName + " components found in scene; disabling this component.", this);
                this.enabled = false;
                return;
            }
        }

        OptitrackRigidBodyState rbState = StreamingClient.GetLatestRigidBodyState(RigidBodyId);
        A = rbState.Markers[0].Position;
        B = rbState.Markers[1].Position;
        C = rbState.Markers[2].Position;
        n = Vector3.Cross(B - A, C - A);

        min = new Vector3(rbState.Pose.Position.x, rbState.Pose.Position.y, rbState.Pose.Position.z);
        max = new Vector3(rbState.Pose.Position.x + 1, rbState.Pose.Position.y + 1, rbState.Pose.Position.z + 1);
        offset = new Vector3(rbState.Pose.Position.x, rbState.Pose.Position.y, rbState.Pose.Position.z);
        offsetRotation = Quaternion.Inverse(rbState.Pose.Orientation);//new Quaternion(rbState.Pose.Orientation.x,rbState.Pose.Orientation.y,rbState.Pose.Orientation.z,rbState.Pose.Orientation.w);


    }


    void Update()
    {


        OptitrackRigidBodyState rbState = StreamingClient.GetLatestRigidBodyState(RigidBodyId);

        if (Input.GetKeyDown(KeyCode.I))
        {
            this.min.Set(rbState.Pose.Position.x, rbState.Pose.Position.y, rbState.Pose.Position.z);
            print("ABWA" + rbState.Pose.Position);
        }
        if (Input.GetKeyDown(KeyCode.O))
        {
            this.max.Set(rbState.Pose.Position.x, rbState.Pose.Position.y, rbState.Pose.Position.z);
            print("ABWA" + rbState.Pose.Position);
        }
        if (rbState != null)
        {
            float coef_x, coef_y, coef_z;
            coef_x = 1 - (rbState.Pose.Position.x - min.x) / (max.x - min.x);
            coef_y = (rbState.Pose.Position.y - min.y) / (max.y - min.y);
            coef_z = (rbState.Pose.Position.z - min.z) / (max.z - min.z);
            float translate_x = coef_x * PMax.x + (1 - coef_x) * PMin.x;
            float translate_y = coef_y * PMax.y + (1 - coef_y) * PMin.y;
            float translate_z = coef_z * PMax.z + (1 - coef_z) * PMin.z;
            Vector3 translate;
            translate.x = translate_x;
            translate.y = translate_y;
            translate.z = translate_z;
            this.transform.localPosition = translate;
            this.transform.localRotation = rbState.Pose.Orientation * offsetRotation;
        }

    }
    

}

